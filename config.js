module.exports = {
  url: 'https://busfactorpp.ru',
  pathPrefix: '/',
  title: 'busfactor++ | devops blog',
  keywords:
    'devops, tutorial, Vasiliy Shakhunov, yandex, aws, cloud',
  subtitle: 'Блог devops инженера',
  copyright: '© 2021',
  disqusShortname: 'icha1',
  postsPerPage: 20,
  googleAnalyticsId: '2250746635',
  yandexMetrikaId: '64359568',
  menu: [
    {
      label: 'Blog',
      path: '/'
    }
  ],
  author: {
    name: 'Vasiliy Shakhunov',
    photo: '/photo.webp',
    bio: 'busfactor++ | devops blog',
    contacts: {
      // don't remove fields, just make them empty string ''
      // https://github.com/gatsbyjs/gatsby/issues/2392
      github: 'webchi',
      twitter: '_ichai',
      linkedin: 'v.shakhunov',
      telegram: '',
      instagram: '',
      facebook: 'v.shakhunov',
      email: '',
      rss: '/rss.xml'
    }
  }
}
