---
template: post
title: Hello Hugo
slug: /posts/hello-hugo
draft: false
priority: 0
date: 2018-12-04T00:05:03+03:00
description: The beginning of this blog is here
category: offtopic
tags:
  - лирика
---



Пробую вести новый блог с блекджеком и генератором статики [hugo](https://gohugo.io/). Надеюсь постов будет больше, чем только этот. Благо голова уже не выдерживает удерживать обилие изученного - надо срочно записывать!