---
template: post
title: "Что такое devops простыми словами?"
slug: /posts/what-is-devops-in-simple-words
draft: false
priority: 0
date: 2019-07-29T14:05:42+03:00
description: Trying to understand about what devops is
category: статьи
tags:
  - основы
---

Жена и коллеги уже просят рассказать, чем я занимаюсь и что такое **devops**. Но как рассказать о том для чего ещё нет общих понятий? Да и времена нынче такие, что все пытаются не давать никаких определений явлениям, а лишь обозначать предметную область.

Как слово "информация". Всегда меня напрягало тем, что ничего не определяет, а просто обозначает "содержания в форме".

![Криминальное чтиво Наши имена нихрена не значат](american_names.png 'Криминальное чтиво Наши имена нихрена не значат')

Так же и "**devops**", наследующий этот порок американской культуры, обозначает некоторый пузырь наполненный понятиями, представлениями и инструментами, о том как должна происходить разработка программного обеспечения.

В настоящее время произошёл качественный скачок в разработке ПО, подобный переходу от мануфактуры к конвейерному производству. И "девопс инженер" обычно обозначается специалиста который умеет собрать такой конвейер непрерывной доставки программного обеспечения на сервера для его работы, а так же настройку самих серверов.

Аббревиатура **Devops** (development operations) обозначает объедение двух ранее разных ролей - разработчика (developers) и системного администратора (operators). Что даёт возможность целиком поддерживать продукт, начиная от разработки, заканчивая эксплуатацией.

Ну и в завершение хорошая иллюстрация из игры StarCraft о том как соединение двух сущностей даёт нечто новое и принципиально инное =)
`youtube: qYnSsmcgvfw`